﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {

	public float fireRate = 0;
	public float Damage = 10;
	public LayerMask notToHit;
	public Transform BulletTrail;
	float timeToSpawnEffect = 0;
	float effectSpawnRate = 10;
	public int shots = 6;

	float timeToFire = 0;
	Transform firePoint;
	// Use this for initialization
	void Awake () {
		firePoint  = transform.FindChild("FirePoint");
	}
	
	// Update is called once per frame
	void Update () {
		
		if (fireRate == 0) {
			if (Input.GetButtonDown ("Fire1")) {
				Shoot ();
			}
		} else {
			if (Input.GetButton ("Fire1") && Time.time > timeToFire) {
				timeToFire = Time.time + 1 / fireRate;
				Shoot ();
			}
		}

	}
	void Shoot (){
		if (shots > 0) {
			Vector2 mousePosistion = new Vector2 (Camera.main.ScreenToWorldPoint (Input.mousePosition).x, Camera.main.ScreenToWorldPoint (Input.mousePosition).y);
			Vector2 firePointPosition = new Vector2 (firePoint.position.x, firePoint.position.y);
			RaycastHit2D hit = Physics2D.Raycast (firePointPosition, mousePosistion - firePointPosition, 100, notToHit);
			if (Time.time >= timeToSpawnEffect) {
				Bang ();
				timeToSpawnEffect = Time.time + 1 / effectSpawnRate;
				shots -= 1;
			}
		} else
			Destroy (gameObject, 1);

	}
	void Bang(){
		Instantiate( BulletTrail, firePoint.position, firePoint.rotation);
	}
}
